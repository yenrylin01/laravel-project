<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new animal("shaun");
    echo "Name : ". $sheep->name . "<br>";
    echo "Legs : ". $sheep->legs . "<br>";
    echo "Cold Blooded : ". $sheep->cold_blooded . "<br><br>";

    $amphibi = new frog("buduk");
    echo "Name : ". $amphibi->name . "<br>";
    echo "Legs : ". $amphibi->legs . "<br>";
    echo "Cold Blooded : ". $amphibi->cold_blooded . "<br>";
    echo "Jump : ". $amphibi->jump . "<br><br>";

    $kera = new ape("kera sakti");
    echo "Name : ". $kera->name . "<br>";
    echo "Legs : ". $kera->legs . "<br>";
    echo "Cold Blooded : ". $kera->cold_blooded . "<br>";
    echo "Yell : ". $kera->yell . "<br><br>";
?>